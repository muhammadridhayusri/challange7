require("dotenv").config();

const express = require('express')
const app = express()
const port = 3000
const { sequelize } = require('./models')
const route = require('./routes')


app.use(express.json())
app.use(express.urlencoded({extended:true}))

app.set('view engine', 'ejs')
app.use(express.static('views/assets/'))

app.use(route)

sequelize.authenticate()
   .then(()=> console.log(`DB CONNECTED`) )
   .catch((err)=> console.log(`${err}`))

app.listen(port,()=> {
   console.log(`port listen on ${port}`)
})
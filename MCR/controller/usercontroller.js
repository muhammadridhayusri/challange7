const {users,choosens,rooms} = require('../models')
const {results} = require('../models')
const {utility} = require('../utility/middleware')

class userController {
   static async createUser(req,res,next) {
      try {
         const {username, password, permission} = req.body
         const data = await users.create({
            userName: username,
            passWord: password,
            permission
         })
         console.log(`create user ${username} success`)
         res.status(200).json({
            msg:`succes`
         })
      } catch (error) {
         console.log(error)
         res.status(400).json({
            msg:`${error}`
         })
      }
   }
   static async getUser(req,res,next) {
      try {
         const data = await users.findAll()
         console.log(`show user success`)
         res.status(200).json({
            msg: data
         })
      } catch (error) {
         console.log(error)
         res.status(400).json({
            msg:`${error}`
         })
      }
   }

   static async createRoom(req,res,next) {
      try {
         const data = await rooms.create()
         res.json({
            msg: 'ROOMS HAS BEEN MADE'
         })
      } catch (error) {
          console.log(error)
         res.status(400).json({
            msg:`${error}`
         })
      }
   }

   static async login(req,res,next) {
      try {
         const {username,password} = req.body
         const data = await users.findAll()
         const _username = data.find(data => data.userName == username)
         if(!_username.userName) {
            res.status(400).json({
               msg: `wrong username or password`
            })}
         const _password = data.find(data => data.passWord == password)
         const _permission = _username.permission
         if(_username.userName == username && _password.passWord == password) {
            console.log(`login sukses for ${username}`)
            const a = utility.getToken(username,_permission)
            res.json({
               msg: `welcome ${username} ,and its authorize by ${_permission}`,
               token: `${a}`
         })
         } else {
            res.status(400).json({
               msg: `wrong username or password`
            })
         }
      } catch (error) {
         console.log(error)
         res.status(400).json({
            msg:`error > ${error}`
         })
      }
   }

   static async playGame(req,res,next) {
      try {
         const {pilihan, roomid} = req.body
         const username = req.username
         const _rooms = await rooms.findAll()
         const _choose = await choosens.findOne({
            where: {RoomId : roomid}
         })
         console.log(_choose)
         if (!_choose) {
            const data = await choosens.create({player1: pilihan,RoomId: roomid})
            console.log(username, `memilih ${pilihan}`)
            res.json({
               msg: 'succes make choose 1'
            })
         } else if(_choose.player1 || _choose.player2) {
            const data = await choosens.update({player2: pilihan},{where: {RoomId: roomid}})
            console.log(username, `memilih ${pilihan}`)
            res.json({
               msg: 'succes make choose 2'
            })
           
         } else {
            console.log('room have full')
            res.json({
               msg: `room have full`
            })
         }
      }   
      catch (error) {
         console.log(error)
         res.status(400).json({
            msg:`${error}`
         })
      }
   }

   static async result (req,res,next) {
      try {
         const _roomid1 = req.params.id
         const _dataplayer = await choosens.findOne({where: {RoomId: _roomid1}})
         console.log(_dataplayer.player1)
         console.log(_dataplayer.player2)
         const _p1 = _dataplayer.player1
         const _p2 = _dataplayer.player2
         if (!_p1 || !_p2) {
            console.log(`data has not been made`)
            res.json({
               msg: ` data has not been made`
            })
         } 
         else if(_p1 == _p2) {
            const hasil = await results.create({results:`draw`,RoomId: _roomid1})
            console.log(hasil)
            console.log(hasil.results)
            res.json({
               msg: `p1 vs p2 is ${hasil.results}`
            })
         } 
         else if(_p1 == `R` && _p2 == `P`) {
            const hasil = await results.create({results:`p2 win`,RoomId : _roomid1})
            console.log(hasil)
            console.log(hasil.results)
            res.json({
               msg: `p1 vs p2 is ${hasil.results}`
            })
         } 
         else if(_p1 == `P` && _p2 == `R`) {
            const hasil = await results.create({results:`p1 win`,RoomId : _roomid1})
            console.log(hasil)
            console.log(hasil.results)
            res.json({
               msg: `p1 vs p2 is ${hasil.results}`
            })
         } 
         else if(_p1 == `R` && _p2 == `S`) {
            const hasil = await results.create({results:`p1 win`,RoomId : _roomid1})
            console.log(hasil)
            console.log(hasil.results)
            res.json({
               msg: `p1 vs p2 is ${hasil.results}`
            })
         } 
         else if(_p1 == `S` && _p2 == `R`) {
            const hasil = await results.create({results:`p2 win`,RoomId : _roomid1})
            console.log(hasil)
            console.log(hasil.results)
            res.json({
               msg: `p1 vs p2 is ${hasil.results}`
            })
         } 
         else if(_p1 == `S` && _p2 ==`P`) {
            const hasil = await results.create({results:`p1 win`,RoomId : _roomid1})
            console.log(hasil)
            console.log(hasil.results)
            res.json({
               msg: `p1 vs p2 is ${hasil.results}`
            })
         }
         else if(_p1 == `P` && _p2 == `S`) {
            const hasil = await results.create({results:`p1 win`,RoomId : _roomid1})
            console.log(hasil)
            console.log(hasil.results)
            res.json({
               msg: `p1 vs p2 is ${hasil.results}`
            })
         } 
         else {
            console.log(`data not defined`)
            res.json({
               msg: `data is not defined`
            })
         }
         
      
    } catch (error) {
         console.log(`err`, `data is not defined`)
         res.status(400).json({
            msg: `err make data first before show the result`
         })
      }
   }
}

module.exports = userController
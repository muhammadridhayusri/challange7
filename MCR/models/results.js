'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class results extends Model {
    static associate(models) {
      results.belongsTo(models.rooms, {foreignKey: 'RoomId'})
    }
  }
  results.init({
    RoomId: DataTypes.INTEGER,
    results: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'results',
  });
  return results;
};
const express = require('express')
const route = express.Router()
const {userController} = require('../controller')
const {utility} = require('../utility/middleware')

route.post('/createuser',userController.createUser)
route.get('/showusers',userController.getUser)
route.post('/login',userController.login)
route.get('/playgame',utility.decodeToken, userController.playGame)
route.post('/createroom',userController.createRoom)
route.post('/resultgame/:id', userController.result)

route.get('*', function(req,res) {
   res.send(`<h1 style='text-align: center;color:red'>WRONG PAGE</h1>`)
})

module.exports = route
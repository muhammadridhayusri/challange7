'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class rooms extends Model {

    static associate(models) {
      rooms.hasOne(models.results, {foreignKey:'RoomId'}),
      rooms.hasOne(models.choosens, {foreignKey:'RoomId'})
    }
  }
  rooms.init({

  }, {
    sequelize,
    modelName: 'rooms',
  });
  return rooms;
};
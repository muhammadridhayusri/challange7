require("dotenv").config();

const express = require('express')
const app = express()
const port = 3000
const {userController} = require('./controller')
const path = require('path')
const { sequelize } = require('./models')
const {utility} = require('./utility/middleware')


app.use(express.json())
app.use(express.urlencoded({extended:true}))

app.set('view engine', 'ejs')
app.use(express.static('views/assets/'))

app.post('/createuser',userController.createUser)
app.get('/showusers',userController.getUser)
app.post('/login',userController.login)
app.get('/playgame',utility.decodeToken, userController.playGame)
app.post('/createroom',userController.createRoom)
app.get('/resultgame/:id', userController.result)
app.get('/showresult',userController.showresult)

sequelize.authenticate()
   .then(()=> console.log(`DB CONNECTED`) )
   .catch((err)=> console.log(`${err}`))

app.listen(port,()=> {
   console.log(`port listen on ${port}`)
})